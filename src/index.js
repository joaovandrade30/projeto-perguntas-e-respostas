const express = require('express');
const app = express();
const port = 8080;


app.get('/', (req, res) => {
  res.send('Welcome to the port ' + port);
});

app.listen(port, (req, res) => {
  console.log('listening on port ' + port);
});